package com.business.orderService.service;

import com.business.orderService.entity.OrderDetail;
import com.business.orderService.entity.OrderMaster;

import java.math.BigInteger;

/**
 * @Description: The implementation of order service .
 * @Author: Mike Dong
 * @Date: 2019/5/30 16:02.
 */
public interface OrderService {

    /**
     * @Description: Add a new order info.
     * @Author: Mike Dong
     * @Date: 2019/5/30 16:09
     * @Param: [orderMaster]
     * @Return: void
     */
    void addOrderInfo(OrderMaster orderMaster);

    /**
     * @Description: Add a new order Detail.
     * @Author: Mike Dong
     * @Date: 2019/6/6 11:34
     * @Param: [orderDetail]
     * @Return: void
     */
    void addOrderDetail(OrderDetail orderDetail);

    /**
     * @Description: update the order status by order number.
     * @Author: Mike Dong
     * @Date: 2019/6/5 17:09
     * @Param: [orderSn, orderStatus]
     * @Return: void
     */
    void updateOrderStatus(BigInteger orderSn, Integer orderStatus);
}
