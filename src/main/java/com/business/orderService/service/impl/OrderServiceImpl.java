package com.business.orderService.service.impl;

import com.business.orderService.entity.OrderDetail;
import com.business.orderService.entity.OrderMaster;
import com.business.orderService.repository.OrderDetailRepository;
import com.business.orderService.repository.OrderRepository;
import com.business.orderService.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @Description: The implementation of order service .
 * @Author: Mike Dong
 * @Date: 2019/5/30 16:02.
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    /**
     * @Description: Add a new order info.
     * @Author: Mike Dong
     * @Date: 2019/5/30 16:10
     * @Param: [orderMaster]
     * @Return: void
     */
    public void addOrderInfo(OrderMaster orderMaster) {
        orderRepository.save(orderMaster);
    }

    /**
     * o
     *
     * @Description: Add a new oder detail.
     * @Author: Mike Dong
     * @Date: 2019/6/6 11:35
     * @Param: [orderDetail]
     * @Return: void
     */
    @Override
    public void addOrderDetail(OrderDetail orderDetail) {
        orderDetail.setModifiedTime(new Timestamp(new Date().getTime()));
        orderDetailRepository.save(orderDetail);
    }

    /**
     * @Description: update the order status by order number.
     * @Author: Mike Dong
     * @Date: 2019/6/5 17:11
     * @Param: [orderSn, orderStatus]
     * @Return: void
     */
    @Override
    public void updateOrderStatus(BigInteger orderSn, Integer orderStatus) {
        orderRepository.updateOrderStatus(orderSn, orderStatus);
    }
}
