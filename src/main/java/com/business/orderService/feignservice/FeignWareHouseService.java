package com.business.orderService.feignservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description: Service for warehouse operations.
 * @Author: Mike Dong
 * @Date: 2019/6/6 16:21.
 */
@FeignClient(value = "warehouse-service", fallback = FeignWareHouseFallBack.class)
public interface FeignWareHouseService {

    @PostMapping(path = "/deductWareHouseProduct")
    void deductWareHouseProduct(@RequestParam("productId") Long productId, @RequestParam("deductCnt") Long deductCnt);
}
