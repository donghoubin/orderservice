package com.business.orderService.controller;

import com.alibaba.fastjson.JSON;
import com.business.orderService.entity.OrderDetail;
import com.business.orderService.entity.OrderMaster;
import com.business.orderService.feignservice.FeignWareHouseService;
import com.business.orderService.service.OrderService;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.apache.rocketmq.common.message.Message;

import java.math.BigInteger;

/**
 * @Description: The controller for order operations.
 * @Author: Mike Dong
 * @Date: 2019/5/30 16:02.
 */
@RestController
public class OrderController {
    Logger log = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @Autowired
    private FeignWareHouseService feignWareHouseService;

    @Autowired
    private DefaultMQProducer producer;

    /**
     * @Description: Add a new order info.
     * @Author: Mike Dong
     * @Date: 2019/5/30 16:20
     * @Param: [orderMaster]
     * @Return: void
     */
    @PostMapping(path = "/addOrderInfo")
    public void addOrderInfo(@RequestBody OrderMaster orderMaster) {
        orderService.addOrderInfo(orderMaster);
    }

    /**
     * @Description: Add a new order Detail.
     * @Author: Mike Dong
     * @Date: 2019/6/6 11:32
     * @Param: [orderDetail]
     * @Return: void
     */
    @PostMapping(path = "/addOrderDetailInfo")
    public void addOrderDetail(@RequestBody OrderDetail orderDetail) {
        orderService.addOrderDetail(orderDetail);
    }

    @GetMapping(path = "/getOrderInfo")
    public String getOrderInfo() {
        return "fff";
    }

    /**
     * @Description: update the order status by order number.
     * @Author: Mike Dong
     * @Date: 2019/6/3 17:03
     * @Param: [orderSn, orderStatus]
     * @Return: void
     */
    @PostMapping(path = "/updateOrderStatus")
    public void updateOrderStatus(@RequestParam("orderSn") BigInteger orderSn, @RequestParam("orderStatus") Integer orderStatus,
                                  @RequestParam("productId") Long productId, @RequestParam("deductCnt") Long deductCnt) throws RemotingException, MQClientException, InterruptedException {
        orderService.updateOrderStatus(orderSn, orderStatus);
        feignWareHouseService.deductWareHouseProduct(productId, deductCnt);

        Message message = new Message("TopicTest", "Tag1", "12345", "rocketmq测试成功".getBytes());
        // 这里用到了这个mq的异步处理，类似ajax，可以得到发送到mq的情况，并做相应的处理
        // 不过要注意的是这个是异步的
        producer.send(message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                log.info("传输成功");
                log.info(JSON.toJSONString(sendResult));
            }

            @Override
            public void onException(Throwable e) {
                log.error("传输失败", e);
            }
        });
    }
}
