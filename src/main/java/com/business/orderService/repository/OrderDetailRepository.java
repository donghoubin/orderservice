package com.business.orderService.repository;

import com.business.orderService.entity.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Description: The repository for oder detail operations.
 * @Author: Mike Dong
 * @Date: 2019/6/6 11:36.
 */
@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long> {
}
