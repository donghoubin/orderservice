package com.business.orderService.repository;

import com.business.orderService.entity.OrderMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;

/**
 * @Description: The repository for oder operations.
 * @Author: Mike Dong
 * @Date: 2019/5/30 16:24.
 */
@Repository
public interface OrderRepository extends JpaRepository<OrderMaster, Long> {

    @Transactional
    @Query("update OrderMaster orderMaster set orderMaster.orderStatus = ?2 where orderMaster.orderSn = ?1")
    @Modifying
    void updateOrderStatus(BigInteger orderSn, Integer orderStatus);
}
