package com.business.orderService.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @Description:
 * @Author: Mike Dong
 * @Date: 2019/6/6 11:07.
 */
@Entity
@Table(name = "order_detail")
public class OrderDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_detail_id", nullable = false, length = 10)
    private Long orderDetailId;

    @Column(name = "order_id", nullable = false, length = 10)
    private Long orderId;

    @Column(name = "product_id", nullable = false, length = 10)
    private Long productId;

    @Column(name="product_name", nullable = false, length = 50)
    private String productName;

    @Column(name="product_cnt", nullable = false, length = 11)
    private Long productCnt;

    @Column(name="product_price", nullable = false, precision = 8, scale = 2)
    private BigDecimal productPrice;

    @Column(name="average_cost", nullable = false, precision = 8, scale = 2)
    private BigDecimal averageCost;

    @Column(name="weight", nullable = true)
    private Float weight;

    @Column(name="fee_money", nullable = false, precision = 8, scale = 2)
    private BigDecimal feeMoney;

    @Column(name="w_id", nullable = false, length = 10)
    private Long wId;

    @Column(name="modified_time", nullable = false)
    @JsonFormat( pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp modifiedTime;

    public Long getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(Long orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getProductCnt() {
        return productCnt;
    }

    public void setProductCnt(Long productCnt) {
        this.productCnt = productCnt;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public BigDecimal getAverageCost() {
        return averageCost;
    }

    public void setAverageCost(BigDecimal averageCost) {
        this.averageCost = averageCost;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public BigDecimal getFeeMoney() {
        return feeMoney;
    }

    public void setFeeMoney(BigDecimal feeMoney) {
        this.feeMoney = feeMoney;
    }

    public Long getwId() {
        return wId;
    }

    public void setwId(Long wId) {
        this.wId = wId;
    }

    public Timestamp getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Timestamp modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
