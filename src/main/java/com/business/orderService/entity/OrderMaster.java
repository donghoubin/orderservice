package com.business.orderService.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

@Entity
@Table(name = "order_master")
public class OrderMaster {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id", nullable = false, length = 10)
    private Long orderId;

    @Column(name = "order_sn", nullable = false, length = 20)
    private BigInteger orderSn;

    @Column(name = "customer_id", nullable = false, length = 10)
    private Long customerId;

    @Column(name = "shipping_user", nullable = false, length = 10)
    private String shippingUser;

    @Column(name = "province", nullable = false, length = 6)
    private Integer province;

    @Column(name = "city", nullable = false, length = 6)
    private Integer city;

    @Column(name = "district", nullable = false, length = 6)
    private Integer district;

    @Column(name = "address", nullable = false, length = 100)
    private String address;

    @Column(name = "payment_method", nullable = false, length = 4)
    private Integer paymentMethod;

    @Column(name = "order_money", nullable = false, precision = 8, scale = 2)
    private BigDecimal orderMoney;

    @Column(name = "district_money", nullable = true, precision = 8, scale = 2)
    private BigDecimal districtMoney;

    @Column(name = "shipping_money", nullable = true, precision = 8, scale = 2)
    private BigDecimal shippingMoney;

    @Column(name = "payment_money", nullable = true, precision = 8, scale = 2)
    private BigDecimal paymentMoney;

    @Column(name = "shipping_comp_name", nullable = true, length = 10)
    private String shippingCompName;

    @Column(name = "shipping_sn", nullable = true, length = 50)
    private String shippingSn;

    @Column(name = "create_time", nullable = true)
    @JsonFormat( pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp createTime;

    @Column(name = "shipping_time", nullable = true)
    @JsonFormat( pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp shippingTime;

    @Column(name = "pay_time", nullable = true)
    @JsonFormat( pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp payTime;

    @Column(name = "receive_time", nullable = true)
    @JsonFormat( pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp receiveTime;

    @Column(name = "order_status", nullable = true, length = 4)
    private Integer orderStatus;

    @Column(name = "order_point", nullable = true, length = 10)
    private Long orderPoint;

    @Column(name = "invoice_time", nullable = true, length = 100)
    private String invoiceTime;

    @Column(name = "modified_time", nullable = false)
    @JsonFormat( pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp modifiedTime;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public BigInteger getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(BigInteger orderSn) {
        this.orderSn = orderSn;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getShippingUser() {
        return shippingUser;
    }

    public void setShippingUser(String shippingUser) {
        this.shippingUser = shippingUser;
    }

    public Integer getProvince() {
        return province;
    }

    public void setProvince(Integer province) {
        this.province = province;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public Integer getDistrict() {
        return district;
    }

    public void setDistrict(Integer district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Integer paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public BigDecimal getOrderMoney() {
        return orderMoney;
    }

    public void setOrderMoney(BigDecimal orderMoney) {
        this.orderMoney = orderMoney;
    }

    public BigDecimal getDistrictMoney() {
        return districtMoney;
    }

    public void setDistrictMoney(BigDecimal districtMoney) {
        this.districtMoney = districtMoney;
    }

    public BigDecimal getShippingMoney() {
        return shippingMoney;
    }

    public void setShippingMoney(BigDecimal shippingMoney) {
        this.shippingMoney = shippingMoney;
    }

    public BigDecimal getPaymentMoney() {
        return paymentMoney;
    }

    public void setPaymentMoney(BigDecimal paymentMoney) {
        this.paymentMoney = paymentMoney;
    }

    public String getShippingCompName() {
        return shippingCompName;
    }

    public void setShippingCompName(String shippingCompName) {
        this.shippingCompName = shippingCompName;
    }

    public String getShippingSn() {
        return shippingSn;
    }

    public void setShippingSn(String shippingSn) {
        this.shippingSn = shippingSn;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getShippingTime() {
        return shippingTime;
    }

    public void setShippingTime(Timestamp shippingTime) {
        this.shippingTime = shippingTime;
    }

    public Timestamp getPayTime() {
        return payTime;
    }

    public void setPayTime(Timestamp payTime) {
        this.payTime = payTime;
    }

    public Timestamp getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(Timestamp receiveTime) {
        this.receiveTime = receiveTime;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Long getOrderPoint() {
        return orderPoint;
    }

    public void setOrderPoint(Long orderPoint) {
        this.orderPoint = orderPoint;
    }

    public String getInvoiceTime() {
        return invoiceTime;
    }

    public void setInvoiceTime(String invoiceTime) {
        this.invoiceTime = invoiceTime;
    }

    public Timestamp getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Timestamp modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
